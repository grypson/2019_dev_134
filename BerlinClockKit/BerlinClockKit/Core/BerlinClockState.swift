//
//  BerlinClockState.swift
//  BerlinClockKit
//
//  Created by Dev 134 on 13/03/2019.
//  Copyright © 2019 Zoo. All rights reserved.
//

import Foundation

public struct BerlinClockState: Equatable {

    public let secondsLamp: BerlinClockLightState
    public let fiveHoursRow: [BerlinClockLightState]
    public let singleHoursRow: [BerlinClockLightState]
    public let fiveMinutesRow: [BerlinClockLightState]
    public let singleMinutesRow: [BerlinClockLightState]

    public var joinedRepresentation: [BerlinClockLightState] {
        return [secondsLamp] + fiveHoursRow + singleHoursRow + fiveMinutesRow + singleMinutesRow
    }

    public var joinedStringRepresentation: String {
        return joinedRepresentation.stringRepresentation
    }

    // Shouldn't be available outside this module because array sizes are not verified here
    init (secondsLamp: BerlinClockLightState,
          fiveHoursRow: [BerlinClockLightState],
          singleHoursRow: [BerlinClockLightState],
          fiveMinutesRow: [BerlinClockLightState],
          singleMinutesRow: [BerlinClockLightState]) {

        self.secondsLamp = secondsLamp
        self.fiveHoursRow = fiveHoursRow
        self.singleHoursRow = singleHoursRow
        self.fiveMinutesRow = fiveMinutesRow
        self.singleMinutesRow = singleMinutesRow
    }

    public init?(from arrayRepresentation: [BerlinClockLightState]) {
        guard arrayRepresentation.count == 24 else { return nil }

        secondsLamp = arrayRepresentation[0]
        fiveHoursRow = Array(arrayRepresentation[1...4])
        singleHoursRow = Array(arrayRepresentation[5...8])
        fiveMinutesRow = Array(arrayRepresentation[9...19])
        singleMinutesRow = Array(arrayRepresentation[20...23])
    }

    public init?(from stringRepresentation: String) {
        guard let arrayRepresentation = stringRepresentation.berlinClockTimeRepresentation else { return nil }
        self.init(from: arrayRepresentation)
    }
}
