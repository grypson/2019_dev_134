//
//  BerlinTime.swift
//  BerlinClockKit
//
//  Created by Dev 134 on 12/03/2019.
//  Copyright © 2019 Zoo. All rights reserved.
//

import Foundation

public struct Time: Equatable {

    public let hour: Int
    public let minutes: Int
    public let seconds: Int

    init(hour: Int, minutes: Int) { // Should be only used in this module
        self.hour = hour
        self.minutes = minutes
        self.seconds = 0
    }

    public init?(hour: Int, minutes: Int, seconds: Int) {
        guard
            (0...23).contains(hour),
            (0...59).contains(minutes),
            (0...59).contains(seconds)
        else { return nil }

        self.hour = hour
        self.minutes = minutes
        self.seconds = seconds
    }

    public init(date: Date, calendar: Calendar = Calendar.current) {
        self.hour = calendar.component(.hour, from: date)
        self.minutes = calendar.component(.minute, from: date)
        self.seconds = calendar.component(.second, from: date)
    }

    public var fullRepresentation: String {
        return "\(String(format: "%02d", hour)):\(String(format: "%02d", minutes)):\(String(format: "%02d", seconds))"
    }

    public var shortRepresentation: String {
        return "\(String(format: "%02d", hour)):\(String(format: "%02d", minutes))"
    }
}
