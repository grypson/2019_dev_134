//
//  BerlinClock.swift
//  BerlinClockKit
//
//  Created by Dev 134 on 12/03/2019.
//  Copyright © 2019 Zoo. All rights reserved.
//

import Foundation

public enum BerlinClockLightState: String {
    case off = "O"
    case yellow = "Y"
    case red = "R"

    public var active: Bool {
        switch self {
        case .off:
            return false
        case .yellow, .red:
            return true
        }
    }
}

public class BerlinClock {

    // MARK: - Full representation

    public func getTime(from clockStringState: String) -> Time? {
        guard let clockState = BerlinClockState(from: clockStringState) else { return nil }
        return getTime(from: clockState)
    }

    public func getTime(from clockArrayState: [BerlinClockLightState]) -> Time? {
        guard let clockState = BerlinClockState(from: clockArrayState) else { return nil }
        return getTime(from: clockState)
    }

    public func getTime(from clockState: BerlinClockState) -> Time {
        let hour = clockState.fiveHoursRow.numberOfActiveLamps * 5 +
            clockState.singleHoursRow.numberOfActiveLamps
        let minutes = clockState.fiveMinutesRow.numberOfActiveLamps * 5 +
            clockState.singleMinutesRow.numberOfActiveLamps

        // We can only check if number of seconds is odd or not, but cant compute exact time
        return Time(hour: hour, minutes: minutes)
    }

    public func getState(time: Time) -> BerlinClockState {
        return BerlinClockState(secondsLamp: getSecondsLampState(time: time),
                                fiveHoursRow: getFiveHoursRowState(time: time),
                                singleHoursRow: getSingleHoursRowState(time: time),
                                fiveMinutesRow: getFiveMinutesRowState(time: time),
                                singleMinutesRow: getSingleMinutesRowState(time: time))
    }

    // MARK: - Seconds lamp

    public func getSecondsLampState(time: Time) -> BerlinClockLightState {
        return getSecondsLampState(seconds: time.seconds)
    }

    private func getSecondsLampState(seconds: Int) -> BerlinClockLightState {
        return seconds % 2 == 0 ? .yellow : .off
    }

    // MARK: - Five hours row

    public func getFiveHoursRowState(time: Time) -> [BerlinClockLightState] {
        return getFiveHoursRowState(hour: time.hour)
    }

    private func getFiveHoursRowState(hour: Int) -> [BerlinClockLightState] {
        let numberOfLights = 4
        var lights = [BerlinClockLightState](repeating: .off, count: numberOfLights)

        let numberOfLightsOn: Int = hour / 5
        for index in 0..<numberOfLights {
            let isActive = index < numberOfLightsOn
            lights[index] = isActive ? .red : .off
        }

        return lights
    }

    // MARK: - Single hours row

    public func getSingleHoursRowState(time: Time) -> [BerlinClockLightState] {
        return getSingleHoursRowState(hour: time.hour)
    }

    private func getSingleHoursRowState(hour: Int) -> [BerlinClockLightState] {
        let numberOfLights = 4
        let hourCount = hour % 5

        let lightsOnArray = [BerlinClockLightState](repeating: .red, count: hourCount)
        let lightsOffArray = [BerlinClockLightState](repeating: .off, count: numberOfLights - hourCount)

        return lightsOnArray + lightsOffArray
    }

    // MARK: - Five minutes row

    public func getFiveMinutesRowState(time: Time) -> [BerlinClockLightState] {
        return getFiveMinutesRowState(minutes: time.minutes)
    }

    private func getFiveMinutesRowState(minutes: Int) -> [BerlinClockLightState] {
        let numberOfLights = 11
        var lights = [BerlinClockLightState](repeating: .off, count: numberOfLights)

        let numberOfLightsOn: Int = minutes / 5
        for index in 0..<numberOfLights {
            let isActive = index < numberOfLightsOn
            let activeColor: BerlinClockLightState = (index + 1) % 3 == 0 ? .red: .yellow
            lights[index] = isActive ? activeColor : .off
        }

        return lights
    }

    // MARK: - Single minutes row

    public func getSingleMinutesRowState(time: Time) -> [BerlinClockLightState] {
        return getSingleMinutesRowState(minutes: time.minutes)
    }

    private func getSingleMinutesRowState(minutes: Int) -> [BerlinClockLightState] {
        let numberOfLights = 4
        let minutesCount = minutes % 5

        let lightsOnArray = [BerlinClockLightState](repeating: .yellow, count: minutesCount)
        let lightsOffArray = [BerlinClockLightState](repeating: .off, count: numberOfLights - minutesCount)

        return lightsOnArray + lightsOffArray
    }

    public init() {}
}
