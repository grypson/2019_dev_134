//
//  BerlinClockLightState+Array.swift
//  BerlinClockKit
//
//  Created by Dev 134 on 13/03/2019.
//  Copyright © 2019 Zoo. All rights reserved.
//

import Foundation

extension Array where Element == BerlinClockLightState {

    public var stringRepresentation: String {
        let stringArray = self.map {
            $0.rawValue
        }
        return stringArray.joined()
    }

    public var numberOfActiveLamps: Int {
        return self.filter { $0.active }.count
    }
}

extension String {

    public var berlinClockTimeRepresentation: [BerlinClockLightState]? {
        let stringRepresentation = self.compactMap { BerlinClockLightState(rawValue: String($0)) }
        return stringRepresentation.count == self.count ? stringRepresentation : nil
    }
}
