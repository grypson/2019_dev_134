//
//  BerlinClockKitTests.swift
//  BerlinClockKitTests
//
//  Created by Dev 134 on 12/03/2019.
//  Copyright © 2019 Zoo. All rights reserved.
//

import XCTest
@testable import BerlinClockKit

class BerlinClockElementsTests: XCTestCase {

    let berlinClock = BerlinClock()

    // MARK: - Single minutes row
    func testSingleMinutesRowCorrect() {
        guard let time = Time(hour: 0, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleMinutesRowCorrect2() {
        guard let time = Time(hour: 23, minutes: 59, seconds: 59) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.yellow, .yellow, .yellow, .yellow]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleMinutesRowCorrect3() {
        guard let time = Time(hour: 12, minutes: 32, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.yellow, .yellow, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleMinutesRowCorrect4() {
        guard let time = Time(hour: 12, minutes: 34, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.yellow, .yellow, .yellow, .yellow]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleMinutesRowCorrect5() {
        guard let time = Time(hour: 12, minutes: 35, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleMinutesRowCorrect6() {
        guard let time = Time(hour: 12, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleMinutesRowState(time: time)
        XCTAssertEqual(lights.count, 4)
    }

    // MARK: - Five minutes rows
    func testFiveMinutesRowCorrect() {
        guard let time = Time(hour: 0, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.off, .off, .off, .off, .off, .off,
                                                      .off, .off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveMinutesRowCorrect2() {
        guard let time = Time(hour: 23, minutes: 59, seconds: 59) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.yellow, .yellow, .red, .yellow, .yellow, .red,
                                                      .yellow, .yellow, .red, .yellow, .yellow]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveMinutesRowCorrect3() {
        guard let time = Time(hour: 12, minutes: 4, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.off, .off, .off, .off, .off, .off,
                                                      .off, .off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveMinutesRowCorrect4() {
        guard let time = Time(hour: 12, minutes: 23, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.yellow, .yellow, .red, .yellow, .off, .off,
                                                      .off, .off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveMinutesRowCorrect5() {
        guard let time = Time(hour: 12, minutes: 35, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveMinutesRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.yellow, .yellow, .red, .yellow, .yellow, .red,
                                                      .yellow, .off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveMinutesRowCorrect6() {
        guard let time = Time(hour: 12, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveMinutesRowState(time: time)
        XCTAssertEqual(lights.count, 11)
    }

    // MARK: - Single hours row
    func testSingleHoursRowCorrect() {
        guard let time = Time(hour: 0, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleHoursRowCorrect2() {
        guard let time = Time(hour: 23, minutes: 59, seconds: 59) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.red, .red, .red, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleHoursRowCorrect3() {
        guard let time = Time(hour: 2, minutes: 4, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.red, .red, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleHoursRowCorrect4() {
        guard let time = Time(hour: 8, minutes: 23, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.red, .red, .red, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleHoursRowCorrect5() {
        guard let time = Time(hour: 14, minutes: 35, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.red, .red, .red, .red]
        XCTAssertEqual(lights, correctLights)
    }
    func testSingleHoursRowCorrect6() {
        guard let time = Time(hour: 12, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getSingleHoursRowState(time: time)
        XCTAssertEqual(lights.count, 4)
    }

    // MARK: - Five hours row
    func testFiveHoursRowCorrect() {
        guard let time = Time(hour: 0, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveHoursRowCorrect2() {
        guard let time = Time(hour: 23, minutes: 59, seconds: 59) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.red, .red, .red, .red]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveHoursRowCorrect3() {
        guard let time = Time(hour: 2, minutes: 4, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveHoursRowCorrect4() {
        guard let time = Time(hour: 8, minutes: 23, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.red, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveHoursRowCorrect5() {
        guard let time = Time(hour: 16, minutes: 35, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveHoursRowState(time: time)
        let correctLights: [BerlinClockLightState] = [.red, .red, .red, .off]
        XCTAssertEqual(lights, correctLights)
    }
    func testFiveHoursRowCorrect6() {
        guard let time = Time(hour: 12, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getFiveHoursRowState(time: time)
        XCTAssertEqual(lights.count, 4)
    }

    // MARK: - Seconds lamp
    func testSecondsLampCorrect() {
        guard let time = Time(hour: 0, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lamp = berlinClock.getSecondsLampState(time: time)
        XCTAssertEqual(lamp, .yellow)
    }
    func testSecondsLampCorrect2() {
        guard let time = Time(hour: 23, minutes: 59, seconds: 59) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lamp = berlinClock.getSecondsLampState(time: time)
        XCTAssertEqual(lamp, .off)
    }
}
