//
//  BerlinClockTests.swift
//  BerlinClockKitTests
//
//  Created by Dev 134 on 13/03/2019.
//  Copyright © 2019 Zoo. All rights reserved.
//

import XCTest
@testable import BerlinClockKit

class BerlinClockTests: XCTestCase {

    let berlinClock = BerlinClock()

    // MARK: - Full state
    func testBerlinClockCorrect() {
        guard let time = Time(hour: 0, minutes: 0, seconds: 0) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getState(time: time).joinedRepresentation
        let correctLights: [BerlinClockLightState] = [.yellow, .off, .off, .off, .off, .off, .off, .off,
                                                      .off, .off, .off, .off, .off, .off, .off, .off,
                                                      .off, .off, .off, .off, .off, .off, .off, .off]
        XCTAssertEqual(lights, correctLights)
    }

    func testBerlinClockCorrect2() {
        guard let time = Time(hour: 23, minutes: 59, seconds: 59) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getState(time: time).joinedStringRepresentation
        let correctLights = "ORRRRRRROYYRYYRYYRYYYYYY"
        XCTAssertEqual(lights, correctLights)
    }

    func testBerlinClockCorrect3() {
        guard let time = Time(hour: 16, minutes: 50, seconds: 06) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getState(time: time).joinedStringRepresentation
        let correctLights = "YRRROROOOYYRYYRYYRYOOOOO"
        XCTAssertEqual(lights, correctLights)
    }

    func testBerlinClockCorrect4() {
        guard let time = Time(hour: 11, minutes: 37, seconds: 01) else {
            XCTFail("Failed to init BerlinTime")
            return
        }

        let lights = berlinClock.getState(time: time).joinedStringRepresentation
        let correctLights = "ORROOROOOYYRYYRYOOOOYYOO"
        XCTAssertEqual(lights, correctLights)
    }

    func testTimeDecoding() {
        let time = berlinClock.getTime(from: "YOOOOOOOOOOOOOOOOOOOOOOO")?.shortRepresentation
        let correctTime = "00:00"
        XCTAssertEqual(time, correctTime)
    }

    func testTimeDecoding2() {
        let time = berlinClock.getTime(from: "ORRRRRRROYYRYYRYYRYYYYYY")?.shortRepresentation
        let correctTime = "23:59"
        XCTAssertEqual(time, correctTime)
    }

    func testTimeDecoding3() {
        let time = berlinClock.getTime(from: "YRRROROOOYYRYYRYYRYOOOOO")?.shortRepresentation
        let correctTime = "16:50"
        XCTAssertEqual(time, correctTime)
    }

    func testTimeDecoding4() {
        let time = berlinClock.getTime(from: "ORROOROOOYYRYYRYOOOOYYOO")?.shortRepresentation
        let correctTime = "11:37"
        XCTAssertEqual(time, correctTime)
    }
}
