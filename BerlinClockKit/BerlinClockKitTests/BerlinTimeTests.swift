//
//  BerlinTimeTests.swift
//  BerlinClockKitTests
//
//  Created by Dev 134 on 12/03/2019.
//  Copyright © 2019 Zoo. All rights reserved.
//

import XCTest
@testable import BerlinClockKit

class BerlinTimeTests: XCTestCase {

    func testBerlinTimeCorrect() {
        let time = Time(hour: 22, minutes: 10, seconds: 16)
        XCTAssertNotNil(time)
    }

    func testBerlinTimeCorrect2() {
        let time = Time(hour: 0, minutes: 0, seconds: 0)
        XCTAssertNotNil(time)
    }

    func testBerlinTimeCorrect3() {
        let time = Time(hour: 23, minutes: 59, seconds: 59)
        XCTAssertNotNil(time)
    }

    func testBerlinTimeCorrect4() {
        let time = Time(date: Date(timeIntervalSince1970: 1273503923)) // 10.05.2010 15:05:23
        let time2 = Time(date: Date(timeIntervalSince1970: 1215788723)) // 11.07.2008 15:05:23
        XCTAssertEqual(time, time2)
    }

    func testBerlinTimeCorrect5() {
        let time = Time(hour: 21, minutes: 37, seconds: 23)
        let time2 = Time(hour: 21, minutes: 37, seconds: 23)
        XCTAssertEqual(time, time2)
    }

    func testBerlinTimeIncorrect() {
        let time = Time(hour: -5, minutes: 5, seconds: 10)
        XCTAssertNil(time)
    }

    func testBerlinTimeIncorrect2() {
        let time = Time(hour: 24, minutes: 5, seconds: 10)
        XCTAssertNil(time)
    }

    func testBerlinTimeIncorrect3() {
        let time = Time(hour: 12, minutes: 60, seconds: 10)
        XCTAssertNil(time)
    }

    func testBerlinTimeIncorrect4() {
        let time = Time(hour: 13, minutes: -12, seconds: 10)
        XCTAssertNil(time)
    }

    func testBerlinTimeIncorrect5() {
        let time = Time(hour: 10, minutes: 5, seconds: 60)
        XCTAssertNil(time)
    }

    func testBerlinTimeIncorrect6() {
        let time = Time(hour: 10, minutes: 5, seconds: -1)
        XCTAssertNil(time)
    }

    func testBerlinTimeIncorrect7() {
        let time = Time(hour: 26, minutes: 62, seconds: 70)
        XCTAssertNil(time)
    }

    func testBerlinTimeIncorrect8() {
        let time = Time(date: Date(timeIntervalSince1970: 1273503923)) // 10.05.2010 15:05:23
        let time2 = Time(date: Date(timeIntervalSince1970: 1215788724)) // 11.07.2008 15:05:24
        XCTAssertNotEqual(time, time2)
    }

    func testBerlinTimeFormatCorrect() {
        let timeString = Time(hour: 05, minutes: 12, seconds: 06)?.fullRepresentation
        let timeString2 = "05:12:06"
        XCTAssertEqual(timeString, timeString2)
    }

    func testBerlinTimeFormatCorrect2() {
        let timeString = Time(hour: 23, minutes: 59, seconds: 59)?.fullRepresentation
        let timeString2 = "23:59:59"
        XCTAssertEqual(timeString, timeString2)
    }

    func testStringRepresentationCorrect() {
        let state: [BerlinClockLightState] = [.off, .red, .yellow, .red, .off]
        XCTAssertEqual(state.stringRepresentation, "ORYRO")
    }

    func testArrayRepresentationCorrect() {
        let state: [BerlinClockLightState] = [.off, .red, .yellow, .red, .off]
        XCTAssertEqual(state, "ORYRO".berlinClockTimeRepresentation)
    }

    func testArrayRepresentationIncorrect() {
        XCTAssertNil("ORYROABC".berlinClockTimeRepresentation)
    }

    func testStateFromString() {
        let state = BerlinClockState(from: "YOOOOOOOOOOOOOOOOOOOOOOO")
        let correctState = BerlinClockState(secondsLamp: .yellow,
                                            fiveHoursRow: [.off, .off, .off, .off],
                                            singleHoursRow: [.off, .off, .off, .off],
                                            fiveMinutesRow: [.off, .off, .off, .off, .off, .off,
                                                             .off, .off, .off, .off, .off],
                                            singleMinutesRow: [.off, .off, .off, .off])
        XCTAssertEqual(state, correctState)
    }

    func testStateFromArray() {
        guard let arrayRepresentation = "YOOOOOOOOOOOOOOOOOOOOOOO".berlinClockTimeRepresentation else {
            XCTFail("Failed to init BerlinClockTimeRepresentation")
            return
        }
        let state = BerlinClockState(from: arrayRepresentation)
        let correctState = BerlinClockState(secondsLamp: .yellow,
                                            fiveHoursRow: [.off, .off, .off, .off],
                                            singleHoursRow: [.off, .off, .off, .off],
                                            fiveMinutesRow: [.off, .off, .off, .off, .off, .off,
                                                             .off, .off, .off, .off, .off],
                                            singleMinutesRow: [.off, .off, .off, .off])
        XCTAssertEqual(state, correctState)
    }
}
