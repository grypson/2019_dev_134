# Berlin Clock Demo iOS #

_Berlin Clock_ for iOS (Apple iPhone)

# Abour project #

 * Project uses seperate module BerlinClockKit for faster compilation time and reusability in other projects
 * View controllers are done in code instead of storyboards. Pros are: faster compilation time, less git conflicts, easier modification for existing views
 * Used architecture is MVVM with Coordinators and reactive binding between view and viewModel

# Getting Setup #

* Install Xcode
* Install Cocoapods
* Run `$ pod install` from the project directory.
* Open Berlin Clock.xcworkspace in Xcode

# Fastlane #

* Install fastlane
* Run `$ fastlane tests` from the Xcode_project directory to run UI and Unit tests.

# Requirements: #
- Xcode 10.0
- CocoaPods