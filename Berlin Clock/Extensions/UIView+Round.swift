//
//  UIView+Round.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit

extension UIView {
    func round() {
        layer.cornerRadius = frame.size.width / 2
        clipsToBounds = true
    }
}
