//
//  UIColor+Components.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit

extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) { //swiftlint:disable:this identifier_name
        self.init(red: r / 255, green: g / 255, blue: b / 255, alpha: 1)
    }
}
