//
//  LayoutConstants.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit

struct LayoutConstants {

    struct BerlinClockLight {
        static let borderWidth: CGFloat = 1.0
    }

    struct BerlinClockView {
        static let rowsSpacing: CGFloat = 3.0
        static let lightsSpacing: CGFloat = 1.0
    }
}
