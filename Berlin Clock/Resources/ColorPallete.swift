//
//  ColorPallete.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit

struct ColorPallete {
    static let backgroundWhite: UIColor = UIColor(r: 239, g: 239, b: 239)
    static let ruby: UIColor = UIColor(r: 150, g: 7, b: 37)
    static let lightYellow: UIColor = UIColor(r: 255, g: 255, b: 150)
}
