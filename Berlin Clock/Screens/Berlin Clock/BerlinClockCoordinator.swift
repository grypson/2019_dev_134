//
//  BerlinClockCoordinator.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit

class BerlinClockCoordinator: ParentCoordinator, ChildCoordinator {

    let appContext: AppContext
    let presenter: UINavigationController
    weak var parentCoordinator: ParentCoordinator?
    var childCoordinators: [Coordinator] = []
    private let viewController: BerlinClockViewController

    func start() {
        presenter.setViewControllers([viewController], animated: false)
    }

    init(presenter: UINavigationController, context: AppContext) {
        self.appContext = context
        self.presenter = presenter

        let viewModel = BerlinClockViewModel()
        self.viewController = BerlinClockViewController(viewModel: viewModel)
    }
}
