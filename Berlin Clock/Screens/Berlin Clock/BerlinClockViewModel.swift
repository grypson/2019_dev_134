//
//  BerlinClockViewModel.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import Foundation
import RxSwift
import BerlinClockKit

class BerlinClockViewModel {

    let title = "Berlin Clock"
    private let disposeBag = DisposeBag()
    private let berlinClock = BerlinClock()
    private var timer: Timer?

    private let timeVariable: Variable<Time> // TODO: - Change to BehaviorRelay
    var timeObservable: Observable<Time> {
        return timeVariable.asObservable()
    }

    private let clockStateVariable: Variable<BerlinClockState> // TODO: - Change to BehaviorRelay
    var clockStateObservable: Observable<BerlinClockState> {
        return clockStateVariable.asObservable()
    }

    @objc private func timerExecute() {
        timeVariable.value = Time(date: Date())
    }

    private func setupObservers() {
        timeObservable
            .subscribe(onNext: { [weak self] time in
                guard let self = self else { return }
                let state = self.berlinClock.getState(time: time)
                self.clockStateVariable.value = state
            }).disposed(by: disposeBag)
    }

    func load() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerExecute), userInfo: nil, repeats: true)
    }

    init() {
        let currentTime = Time(date: Date())
        timeVariable = Variable<Time>(currentTime)
        clockStateVariable = Variable<BerlinClockState>(berlinClock.getState(time: currentTime))
        setupObservers()
    }
}
