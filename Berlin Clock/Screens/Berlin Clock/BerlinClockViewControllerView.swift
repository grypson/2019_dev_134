//
//  BerlinClockViewControllerView.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit

class BerlinClockViewControllerView: UIView {

    lazy var berlinClockView: BerlinClockView = {
        return BerlinClockView()
    }()

    lazy var timeLabel: UILabel = {
        let label = UILabel()

        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.textColor = .black

        return label
    }()

    init() {
        super.init(frame: CGRect.zero)

        backgroundColor = ColorPallete.backgroundWhite

        addSubview(berlinClockView)
        addSubview(timeLabel)

        berlinClockView.anchorCenterYToSuperview()
        berlinClockView.anchor(nil, left: leftAnchor, bottom: nil,
                               right: rightAnchor, topConstant: 0,
                               leftConstant: 20, bottomConstant: 0,
                               rightConstant: 20, widthConstant: 0, heightConstant: 0)
        berlinClockView.heightAnchor.constraint(equalTo: berlinClockView.widthAnchor, multiplier: 1.2).isActive = true

        timeLabel.anchor(berlinClockView.bottomAnchor, left: leftAnchor, bottom: safeAreaLayoutGuide.bottomAnchor,
                         right: rightAnchor, topConstant: 20, leftConstant: 10, bottomConstant: 0,
                         rightConstant: 10, widthConstant: 0, heightConstant: 0)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
