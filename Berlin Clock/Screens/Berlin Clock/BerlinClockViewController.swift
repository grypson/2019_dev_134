//
//  BerlinClockViewController.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit
import RxSwift

class BerlinClockViewController: UIViewController {

    private let disposeBag = DisposeBag()
    private let myView: BerlinClockViewControllerView
    private let viewModel: BerlinClockViewModel

    private func setupBindings() {
        viewModel.timeObservable
            .subscribe(onNext: { [weak self] time in
                self?.myView.timeLabel.text = time.fullRepresentation
            }).disposed(by: disposeBag)

        viewModel.clockStateObservable
            .subscribe(onNext: { [weak self] state in
                self?.myView.berlinClockView.configure(with: state, animated: true)
            }).disposed(by: disposeBag)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.title

        viewModel.load()
    }

    override func loadView() {
        view = myView
    }

    init(viewModel: BerlinClockViewModel) {
        self.myView = BerlinClockViewControllerView()
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)

        setupBindings()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
