//
//  BerlinClockLightView.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit
import BerlinClockKit

class BerlinClockLightView: UIView {

    enum Style {
        case circlular
        case rectangular
    }

    private let style: Style

    private func setState(_ state: BerlinClockLightState) {
        switch state {
        case .off:
            backgroundColor = .white
        case .red:
            backgroundColor = ColorPallete.ruby
        case .yellow:
            backgroundColor = ColorPallete.lightYellow
        }
    }

    func setState(_ state: BerlinClockLightState, animated: Bool = false) {
        animated ? UIView.animate(withDuration: 0.25) {
            self.setState(state)
        } : setState(state)
    }

    private func setupView() {
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = LayoutConstants.BerlinClockLight.borderWidth
        setState(.off)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        switch style {
        case .circlular:
            round()
        case .rectangular:
            layer.cornerRadius = frame.height * 0.1
        }
    }

    init(style: Style = .rectangular) {
        self.style = style
        super.init(frame: CGRect.zero)

        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
