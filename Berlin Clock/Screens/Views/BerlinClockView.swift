//
//  BerlinClockView.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit
import BerlinClockKit

class BerlinClockView: UIView {

    private lazy var verticalStackView: UIStackView = {
        let view = UIStackView()

        view.axis = .vertical
        view.distribution = .fillEqually
        view.spacing = LayoutConstants.BerlinClockView.rowsSpacing

        return view
    }()

    private let secondsLampView: BerlinClockLightView
    private let fiveHoursLampViews: [BerlinClockLightView]
    private let singleHoursLampViews: [BerlinClockLightView]
    private let fiveMinutesLampViews: [BerlinClockLightView]
    private let singleMinutesLampViews: [BerlinClockLightView]

    func configure(with state: BerlinClockState, animated: Bool = false) {
        secondsLampView.setState(state.secondsLamp)
        for (index, state) in state.fiveHoursRow.enumerated() {
            fiveHoursLampViews[index].setState(state, animated: animated)
        }
        for (index, state) in state.singleHoursRow.enumerated() {
            singleHoursLampViews[index].setState(state, animated: animated)
        }
        for (index, state) in state.fiveMinutesRow.enumerated() {
            fiveMinutesLampViews[index].setState(state, animated: animated)
        }
        for (index, state) in state.singleMinutesRow.enumerated() {
            singleMinutesLampViews[index].setState(state, animated: animated)
        }
    }

    private func setupView() {
        addSubview(secondsLampView)
        addSubview(verticalStackView)

        secondsLampView.anchorCenterXToSuperview()
        secondsLampView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        secondsLampView.widthAnchor.constraint(equalTo: secondsLampView.heightAnchor, multiplier: 1).isActive = true
        secondsLampView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.25).isActive = true

        verticalStackView.anchor(secondsLampView.bottomAnchor, left: leftAnchor, bottom: bottomAnchor,
                                 right: rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0,
                                 rightConstant: 0, widthConstant: 0, heightConstant: 0)

        let fiveHoursStack = UIStackView(arrangedSubviews: fiveHoursLampViews)
        let singleHoursStack = UIStackView(arrangedSubviews: singleHoursLampViews)
        let fiveMinutesStack = UIStackView(arrangedSubviews: fiveMinutesLampViews)
        let singleMinutesStack = UIStackView(arrangedSubviews: singleMinutesLampViews)

        [fiveHoursStack, singleHoursStack, fiveMinutesStack, singleMinutesStack].forEach {
            $0.axis = .horizontal
            $0.distribution = .fillEqually
            $0.spacing = LayoutConstants.BerlinClockView.lightsSpacing
            verticalStackView.addArrangedSubview($0)
        }
    }

    override init(frame: CGRect) {
        secondsLampView = BerlinClockLightView(style: .circlular)
        fiveHoursLampViews = (1...4).map { _ in BerlinClockLightView() }
        singleHoursLampViews = (1...4).map { _ in BerlinClockLightView() }
        fiveMinutesLampViews = (1...11).map { _ in BerlinClockLightView() }
        singleMinutesLampViews = (1...4).map { _ in BerlinClockLightView() }

        super.init(frame: frame)

        backgroundColor = .clear
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
