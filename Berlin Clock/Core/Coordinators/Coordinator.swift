//
//  coordinator.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import Foundation

protocol Coordinator: class {
    var appContext: AppContext { get }
    func start()
}
