//
//  ChildCoordinator.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import Foundation

protocol ChildCoordinator: Coordinator {
    var parentCoordinator: ParentCoordinator? { get set }
}

extension ChildCoordinator {

    func removeFromParentCoordinator() {
        parentCoordinator?.remove(childCoordinator: self)
    }
}

extension ChildCoordinator where Self: ParentCoordinator {

    func removeFromParentCoordinator() {
        removeAllChildCoordinators()
        parentCoordinator?.remove(childCoordinator: self)
    }
}
