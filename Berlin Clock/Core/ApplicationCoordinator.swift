//
//  ApplicationCoordinator.swift
//  Berlin Clock
//
//  Created by Dev 134 on 14/03/2019.
//

import UIKit

class ApplicationCoordinator: ParentCoordinator {

    let appContext: AppContext
    let window: UIWindow
    let navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []

    init(window: UIWindow) {
        self.window = window
        appContext = AppContext()
        navigationController = UINavigationController()
    }

    func start() {
        window.rootViewController = navigationController
        window.makeKeyAndVisible()

        let homeCoordinator = BerlinClockCoordinator(presenter: navigationController, context: appContext)
        add(childCoordinator: homeCoordinator)
    }
}
